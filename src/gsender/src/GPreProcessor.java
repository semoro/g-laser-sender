package gsender.src;

import com.sun.javafx.geom.Vec3f;

import javax.swing.*;
import java.awt.*;
import java.io.InputStream;
import java.util.*;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by 1 on 29.01.2015.
 */
public class GPreProcessor {
    String[] aCodes={
            "G00","G01","G02","G03"
    };
    public GPreProcessor(Stream<String> proc){
        List<String> code_tmp=(proc.map(s -> s.replaceAll("\\(.*\\)", "")).filter(s -> s.length() > 0).filter(line -> {
            for (String gcode : aCodes)
                if (line.startsWith(gcode))
                    return true;
            return false;
        }).collect(Collectors.toList()));
        code=new LinkedList<>();
        Iterator<String> iter=code_tmp.iterator();
        while (iter.hasNext()){
            String line=iter.next();
            String[] split=line.split(" ");
            HashMap args=new HashMap();
            for(String s:split)
                args.put(s.charAt(0),s.substring(1));
            processGCode(args);
        }
       // TaskForm.open(code);
    }

    private float pf(String s,float or){
        return s==null?or:Float.parseFloat(s);
    }

    public Vec3f getPos(HashMap<Character,String> args){
        return new Vec3f(pf(args.get('X'),pos.x),pf(args.get('Y'),pos.y),pf(args.get('Z'),pos.z));
    }

    private void line(Vec3f np){
        code.add("G00 X"+np.x+" Y"+np.y+" Z"+np.z+" F"+feedrate);
    }


    Vec3f pos=new Vec3f();
    float feedrate=1000;
    private void processGCode(HashMap<Character,String> args){
        feedrate=pf(args.get('F'),feedrate);
        switch (args.get('G')){
            case "00": {
                Vec3f np = getPos(args);
                line(np);
                pos.set( np);
                break;
            }
            case "01":{
                Vec3f np=getPos(args);
                line(np);
                pos.set(np);
                break;}
            case "02": {
                Vec3f end = getPos(args);
                Vec3f delta= new Vec3f(pf(args.get('I'),0),pf(args.get('J'),0),0);
                Vec3f center=new Vec3f(pos);
                center.add(delta);
                end.sub(center);
                double ang1=Math.atan2(-delta.y,-delta.x);
                double ang2=Math.atan2(end.y,end.x);
                if(ang2>=ang1)
                    ang2-=2*Math.PI;
                float rad=delta.length();
                Vec3f np=new Vec3f();
                for(double a=ang1;a>=ang2;a-=2f/rad){
                    np.x=(float)(Math.cos(a)*rad+center.x);
                    np.y=(float)(Math.sin(a)*rad+center.y);
                    line(np);
                    pos.set(np);
                }
                end.add(center);
                line(np);
                pos.set(end);
                break;
            }
            case "03":{
                Vec3f end = getPos(args);
                Vec3f delta= new Vec3f(pf(args.get('I'),0),pf(args.get('J'),0),0);
                Vec3f center=new Vec3f(pos);
                center.add(delta);
                end.sub(center);
                double ang1=Math.atan2(-delta.y,-delta.x);
                double ang2=Math.atan2(end.y,end.x);
                if(ang2<=ang1)
                    ang2+=2*Math.PI;
                float rad=delta.length();
                Vec3f np=new Vec3f();
                for(double a=ang1;a<=ang2;a+=2f/rad){
                    np.x=(float)(Math.cos(a)*rad+center.x);
                    np.y=(float)(Math.sin(a)*rad+center.y);
                    line(np);
                    pos.set(np);
                }
                end.add(center);
                line(np);
                pos.set(end);
            }
        }

    }


    public void openTaskForm(){
        TaskForm.open(code);
    }
    private GPreProcessor(){}
    public GPreProcessor copy(){
        GPreProcessor p=new GPreProcessor();
        p.code=new LinkedList<>(code);
        return p;
    }
    public LinkedList<String> code;
    public boolean hasNext(){
       return code.size()>0;
    }
    public String next(){
        if(hasNext())
            return code.removeFirst();
        return null;
    }
}
