package gsender.src;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

/**
 * Created by 1 on 29.01.2015.
 */
public class TaskForm {
    private JPanel root;
    private JProgressBar progressBar1;
    private JList tasks;
    private JButton intr;
    private JButton pause;
    private static class TasksListModel extends AbstractListModel<String>{
        private static class CodeTask{
            public CodeTask(String code) {
                this.code = code;
            }

            public String code;
            boolean started;
            boolean ended;
        }
        LinkedList<CodeTask> data;
        public TasksListModel(LinkedList<String> t){
            data=new LinkedList();
            for(String s:t)
                data.add(new CodeTask(s));
        }
        @Override
        public int getSize() {
            return data.size();
        }

        @Override
        public String getElementAt(int index) {
            CodeTask task=data.get(index);
            return task.code+" "+(task.started?"[GO]":"")+" "+(task.ended?"[OK]":"");
        }
        public void nextGo(){
            for(CodeTask task:data)
                if(!task.started){
                    task.started=true;
                    break;
                }

        }
        public void nextOk(){
            for(CodeTask task:data)
                if(!task.ended){
                    task.ended=true;
                    break;
                }
        }
    }
    TasksListModel model;
    public TaskForm(LinkedList<String> data){
        model=new TasksListModel(data);
        tasks.setModel(model);
        intr.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
            }
        });
    }




    public static void nextGo(){
        if(instance!=null) {

            instance.model.nextGo();
            instance.tasks.updateUI();
        }
    }
    public static void nextOk(){
        if(instance!=null) {
            instance.model.nextOk();
            instance.tasks.updateUI();
        }
    }
    public static TaskForm instance;
    public static void open(LinkedList<String> data){
        instance=new TaskForm(data);
        JFrame frame = new JFrame("TaskForm");
        frame.setContentPane(instance.root);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

}
