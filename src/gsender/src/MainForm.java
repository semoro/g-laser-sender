package gsender.src;

import jssc.*;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.DefaultCaret;
import java.awt.event.*;
import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by 1 on 20.01.2015.
 */
public class MainForm {
    private JButton autohome;
    private JButton intr;
    private JTextArea log;
    private JPanel root;
    private JButton print;
    private JTextField command;
    private JButton send;
    private JTextField file;
    private JScrollPane jsp;
    private JButton pause;
    private JButton prerender;
    private JButton disconnectButton;

    public JFrame frame;

    public JFileChooser jfc;
    File toUpload;

    public MainForm() {


        log.setText("");
        jfc = new JFileChooser();
        jfc.removeChoosableFileFilter(jfc.getAcceptAllFileFilter());
        jfc.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                String fn = f.getName();
                if (fn.lastIndexOf('.') > 0) {
                    String n = fn.substring(fn.lastIndexOf('.') + 1);
                    return n.equalsIgnoreCase("gcode") || n.equalsIgnoreCase("gco");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "G-Code";
            }
        });
        DefaultCaret caret = (DefaultCaret) log.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sendCommand(command.getText());
                } catch (SerialPortException e1) {
                    e1.printStackTrace();
                }
            }
        });
        autohome.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sendCommand("D2");
                } catch (SerialPortException e1) {
                    e1.printStackTrace();
                }
            }
        });
        intr.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    processor = null;
                    sendCommand("D0");
                } catch (SerialPortException e1) {
                    e1.printStackTrace();
                }
            }
        });
        file.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                if (jfc.showOpenDialog(frame) == 0) {
                    toUpload = jfc.getSelectedFile();
                    file.setText(toUpload.getAbsolutePath());
                }
            }

        });
        print.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sendFile(toUpload);

                } catch (SerialPortException e1) {
                    e1.printStackTrace();
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });

        pause.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (paused) {
                        sendCommand("D5");
                        pause.setText("Pause");
                        paused = false;
                    } else {
                        sendCommand("D4");
                        pause.setText("Unpause");
                        paused = true;
                    }
                } catch (SerialPortException e1) {
                    e1.printStackTrace();
                }
            }
        });
        prerender.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(toUpload);
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                BufferedReader br=new BufferedReader(new InputStreamReader(fis));


                PreRender.open(new GPreProcessor(br.lines()));

            }
        });
        command.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if(e.getKeyChar()==KeyEvent.VK_ENTER){
                    try {
                        sendCommand(command.getText());
                    } catch (SerialPortException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        disconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if(disconnectButton.getText().equals("Disconnect")){
                        serialPort.closePort();
                        disconnectButton.setText("Connect");
                    } else {
                        openPort();
                        disconnectButton.setText("Disconnect");
                    }

                } catch (SerialPortException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }
    static boolean paused=false;
    static MainForm instance;



    public static void main(String[] args) throws SerialPortException {
        MainForm mf=instance=new MainForm();
        JFrame frame = new JFrame("MainForm");
        frame.setContentPane(mf.root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        Object[] o= SerialPortList.getPortNames();
        if(o.length==0) {
            JOptionPane.showMessageDialog(frame,"COM port not found!","Error",JOptionPane.ERROR_MESSAGE);
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            return;
        }
        Object res=JOptionPane.showInputDialog(frame,"Select COM port","COM port select.",JOptionPane.QUESTION_MESSAGE,null,o,o[0]);
        if(res==null) {
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            return;
        }
        mf.init((String) res);
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                try {
                if(serialPort!=null && serialPort.isOpened())
                    serialPort.closePort();
                } catch (SerialPortException e) {
                    e.printStackTrace();
                }
            }
        });
        mf.frame=frame;
    }




    public static SerialPort serialPort;

    public static String bufferString="";
    public  void onDataReady(String data) throws InterruptedException {

        try {

            System.out.print(data);
            try {
                log.append(data);
                log.setCaretPosition(log.getDocument().getLength());
            }catch (Exception e){
                ;
            }
            if (data.contains("go.")){


              //  TaskForm.nextGo();
            }
            if (data.contains("ok.") && processor!=null && processor.hasNext()) {
              //  TaskForm.nextOk();
                Thread.sleep(60);
                serialPort.writeString(processor.next() + "\n");
            }
        }
        catch (SerialPortException ex) {
            System.out.println(ex);
        }
    }




    private void createUIComponents() {
    }

    public void onDataIncome(String data) throws InterruptedException {
        String[] split=data.split("\n");
        for(int i=0;i<split.length-1;i++){
                onDataReady(split[i]+"\n");
        }
        if(data.charAt(data.length()-1)=='\n') {
            onDataReady(split[split.length - 1]+"\n");
            bufferString="";
        }else
            bufferString=split[split.length-1];
    }

    public class PortReader implements SerialPortEventListener {

        public void serialEvent(SerialPortEvent event) {
            if(event.isRXCHAR() && event.getEventValue() > 0){
                try {
                    String data = serialPort.readString(event.getEventValue());
                    bufferString+=data;
                    if(bufferString.contains("\n")){
                        onDataIncome(bufferString);
                    }

                } catch (SerialPortException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    GPreProcessor processor;
    public void sendFile(File f) throws SerialPortException, FileNotFoundException {

        FileInputStream fis =new FileInputStream(f);
        BufferedReader br=new BufferedReader(new InputStreamReader(fis));

        processor=new GPreProcessor(br.lines());
        if(processor.hasNext())
            serialPort.writeString(processor.next() + "\n");
    }

    public void sendCommand(String cmd) throws SerialPortException {
        serialPort.writeString(cmd+"\n");
    }

    public void openPort(){
        try {

            //Открываем порт
            serialPort.openPort();
            //Выставляем параметры
            serialPort.setParams(SerialPort.BAUDRATE_115200,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            //Включаем аппаратное управление потоком
            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN |
                    SerialPort.FLOWCONTROL_RTSCTS_OUT);
            //Устанавливаем ивент лисенер и маску
            serialPort.addEventListener(new PortReader(), SerialPort.MASK_RXCHAR);

        }
        catch (SerialPortException ex) {
            System.out.println(ex);
        }
    }

    public void init(String port) throws SerialPortException {
        serialPort = new SerialPort(port);

        openPort();
    }
}
